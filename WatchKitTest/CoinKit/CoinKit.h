//
//  CoinKit.h
//  CoinKit
//
//  Created by Chayel Heinsen  on 6/9/15.
//  Copyright © 2015 Spry Group LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoinKit.
FOUNDATION_EXPORT double CoinKitVersionNumber;

//! Project version string for CoinKit.
FOUNDATION_EXPORT const unsigned char CoinKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoinKit/PublicHeader.h>


