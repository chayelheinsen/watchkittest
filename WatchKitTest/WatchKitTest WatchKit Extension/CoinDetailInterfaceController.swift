//
//  CoinDetailInterfaceController.swift
//  WatchKitTest
//
//  Created by Chayel Heinsen  on 6/9/15.
//  Copyright © 2015 Spry Group LLC. All rights reserved.
//

import WatchKit
import Foundation
import CoinKit

class CoinDetailInterfaceController: WKInterfaceController {
    
    var coin: Coin!
    let defaults = NSUserDefaults.standardUserDefaults()
    let favoriteCoinKey = "favoriteCoin"
    
    @IBOutlet weak var coinImage: WKInterfaceImage!
    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    @IBOutlet weak var table: WKInterfaceTable!
    @IBOutlet weak var favoriteSwitch: WKInterfaceSwitch!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        if let coin = context as? Coin {
            self.coin = coin
            
            coinImage.setImageNamed(coin.name)
            nameLabel.setText(coin.name)
            
            let titles = ["Current Price", "Yesterday's Price", "Volume"]
            let values = [String(format: "%.2f USD", coin.price), String(format: "%.2f USD", coin.price24h), String(format: "%.4f", coin.volume)]
            
            table.setNumberOfRows(titles.count, withRowType: "CoinTableRow")
            
            for i in 0..<titles.count {
                
                if let row = table.rowControllerAtIndex(i) as? CoinTableRow {
                    row.titleLabel.setText(titles[i])
                    row.detailLabel.setText(values[i])
                }
            }
            
            if let favoriteCoin = defaults.stringForKey(favoriteCoinKey) {
                favoriteSwitch.setOn(favoriteCoin == coin.name)
            }
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func favoriteSwitchValueChanged(value: Bool) {
        
        if let coin = coin {
            defaults.removeObjectForKey(favoriteCoinKey)
            
            if value {
                defaults.setObject(coin.name, forKey: favoriteCoinKey)
            }
            
            defaults.synchronize()
        }
    }
}
