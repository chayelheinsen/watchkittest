//
//  ExtensionDelegate.swift
//  WatchKitTest WatchKit Extension
//
//  Created by Chayel Heinsen  on 6/9/15.
//  Copyright © 2015 Spry Group LLC. All rights reserved.
//

import WatchKit
import WatchConnectivity
import CoinKit

class ExtensionDelegate: NSObject, WKExtensionDelegate, WCSessionDelegate {

    func applicationDidFinishLaunching() {
        
        if WCSession.isSupported() {
            let session: WCSession = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
        
    }

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }
    
    func sessionReachabilityDidChange(session: WCSession) {
        //  The other app must be reachable before you can use interactive messaging to communicate with it.
        if session.reachable {
            // is reachable
        } else {
            // not reachable
        }
    }
    
    func session(session: WCSession, didReceiveUserInfo userInfo: [String : AnyObject]) {
        
        let coinHelper: CoinHelper = CoinHelper()
        
        if let coinData = userInfo["cachedCoins"] as? NSData {
            
            if let coins = NSKeyedUnarchiver.unarchiveObjectWithData(coinData) as? [Coin] {
                coinHelper.cachePriceData(coins)
                
            }
        }
        
        if let coinData = userInfo["newCoins"] as? NSData {
            
            if let coins = NSKeyedUnarchiver.unarchiveObjectWithData(coinData) as? [Coin] {
                coinHelper.cachePriceData(coins)
                NSNotificationCenter.defaultCenter().postNotificationName("NewCoinData", object: nil)
            }
        }
        
    }
    
}
