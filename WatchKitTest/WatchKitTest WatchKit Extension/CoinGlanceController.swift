//
//  GlanceController.swift
//  WatchKitTest WatchKit Extension
//
//  Created by Chayel Heinsen  on 6/9/15.
//  Copyright © 2015 Spry Group LLC. All rights reserved.
//

import WatchKit
import Foundation
import CoinKit

class CoinGlanceController: WKInterfaceController {
    
    let coinHelper = CoinHelper()
    let defaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var coinImage: WKInterfaceImage!
    @IBOutlet weak var nameLabel: WKInterfaceLabel!
    @IBOutlet weak var priceLabel: WKInterfaceLabel!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Ask the defaults for the user’s favorite currency.
        // It’s possible the user hasn’t selected a favorite and the defaults will return nil.
        // To cover this case, you use the nil coalescing operator to select Dogecoin by default.
        let favoriteCoin = defaults.stringForKey("favoriteCoin") ?? "DOGE"
        let coins = coinHelper.cachedPrices()
        
        for coin in coins {
            
            if coin.name == favoriteCoin {
                coinImage.setImageNamed(coin.name)
                nameLabel.setText(coin.name)
                priceLabel.setText("\(coin.price)")
                updateUserActivity("com.wearespry.WatchKitTest.glance", userInfo: ["coin": coin.name], webpageURL: nil)
                break
            }
        }

    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
