//
//  CoinTableRow.swift
//  WatchKitTest
//
//  Created by Chayel Heinsen  on 6/9/15.
//  Copyright © 2015 Spry Group LLC. All rights reserved.
//

import UIKit
import WatchKit

class CoinTableRow: NSObject {
    @IBOutlet  var titleLabel: WKInterfaceLabel!
    @IBOutlet  var detailLabel: WKInterfaceLabel!
}
