//
//  InterfaceController.swift
//  WatchKitTest WatchKit Extension
//
//  Created by Chayel Heinsen  on 6/9/15.
//  Copyright © 2015 Spry Group LLC. All rights reserved.
//

import WatchKit
import Foundation
import CoinKit

class InterfaceController: WKInterfaceController {
    
    var coins: [Coin] = [Coin]()
    let coinHelper: CoinHelper = CoinHelper()
    
    @IBOutlet var coinTable: WKInterfaceTable!

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "receivedNewCoinData", name: "NewCoinData", object: nil)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        coins = coinHelper.cachedPrices()
        reloadTable()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
        
        if segueIdentifier == "CoinDetails" {
            let coin = coins[rowIndex]
            return coin
        }
        
        return nil
    }
    
    override func handleUserActivity(userInfo: [NSObject : AnyObject]!) {
    
        if let handedCoin = userInfo["coin"] as? String {
            let coins = coinHelper.cachedPrices()
            
            for coin in coins {
                
                if coin.name == handedCoin {
                    pushControllerWithName("CoinDetailInterfaceController", context: coin)
                    break
                }
            }
        }
    }
    
    // MARK: - Helpers
    
    func reloadTable() {
        /**
        *   There’s a bug in the current version of WatchKit where setting the number of rows to
        *   the same number already in a table causes the rows to act strangely.
        *   The workaround is what you see above – checking the existing number of rows before setting it.
        */
        if coinTable.numberOfRows != coins.count {
            coinTable.setNumberOfRows(coins.count, withRowType: "CoinTableRow")
        }
        
        for (index, coin) in coins.enumerate() {
            
            if let row = coinTable.rowControllerAtIndex(index) as? CoinTableRow {
                row.titleLabel.setText(coin.name)
                row.detailLabel.setText("\(coin.price)")
            }
        }
    }
    
    func receivedNewCoinData() {
        coins = coinHelper.cachedPrices()
        reloadTable()
    }
    
}
