//
//  ViewController.swift
//  WatchKitTest
//
//  Created by Chayel Heinsen  on 6/9/15.
//  Copyright © 2015 Spry Group LLC. All rights reserved.
//

import UIKit
import WatchConnectivity
import CoinKit

class ViewController: UIViewController {
    var coins = [Coin]()
    let coinHelper = CoinHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        coins = coinHelper.cachedPrices()
        
        if WCSession.isSupported() {
            let session: WCSession = WCSession.defaultSession()
            session.transferUserInfo(["cachedCoins" : NSKeyedArchiver.archivedDataWithRootObject(coins)])
        }
        
        coinHelper.requestPrice { (coins, error) -> () in
           
            if let coins = coins {
                self.coins = coins
                
                if WCSession.isSupported() {
                    let session: WCSession = WCSession.defaultSession()
                    session.transferUserInfo(["newCoins" : NSKeyedArchiver.archivedDataWithRootObject(coins)])
                }
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

